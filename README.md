Button to Game

a tutorial in steps

see more at this [GIT LAB page](https://tutorials6.gitlab.io/processing_p5js/)

or original at my [ BLOG ](http://kll.engineering-news.org/kllfusion01/articles.php?article_id=166)

in/the

**JEOPARDY_kll**

directory is the JAVA Processing ( first version ) of this tutorial

// developed under WIN 10 / Processing 3.5.3

// made each step in a PDE TAB using some class wrapper to get them all running

the tutorial steps can be selected by arrow keys [ arrow RIGHT ][ arrow LEFT ]

________

now the remake ( with each lesson as extra project )

under 

**single step JAVA**

**single step P5JS**

( while here find the sketch code please also find it at the online editor in a runtime environment )

[link page](https://tutorials6.gitlab.io/processing_p5js/selection.html)