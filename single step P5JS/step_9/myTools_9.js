/*
 myTools  KLL
 use extra js file here in assets
 name: myTools.js
 idea is to develop it here ( master ) and download / upload to other projects.

//_________________________________ !!
 need 
     script src="assets/myTools_9.js"  /script
 inside index.html

content: 
 __________________________________ functions to call:

 logo(x,y,radius,speed_angle_change_per_frame);

*/

//___________________________________________________
//  logo(x,y,radius,speed_angle_change_per_frame);

var ang = 0; //____________________ animated LOGO

function logo(x, y, r, dang) {
  var d1 = 2 * r;
  var d2 = 2 * r / sqrt(2);
  ang += dang; //__________________ animation
  push();
  fill(255); //____________________ fill same all 3 shapes
  strokeWeight(4);
  stroke(200, 200, 0); //__________ here same as main background gives a nice effect
  ellipseMode(CENTER);
  rectMode(CENTER);
  translate(x + r, y + r); //______ CENTER thinking
  push();
  rotate(-ang); //__________________ animate first bigger rect
  rect(0, 0, d1, d1);
  pop();
  ellipse(0, 0, d1, d1);
  rotate(ang); //_________________ animate second smaller rect  
  rect(0, 0, d2, d2);
  textAlign(CENTER,CENTER);
  textSize(20);
  text("K L L",0,0);
  pop();
}

/*
 __________________________________ classes declared:
 
 class Button
 declare:
  Button button(x, y, w, h, sel, text, id);
 methods:
  button.draw(); //_______________ from draw
  button.mousePressed(); //_______ local
  boolean button.over(); //_______ local
  
  sets:
  q_mysel //______________________ a needed global for the 30 buttons page
*/

class Button { //____________________________________ begin class
  constructor(xi, yi, wi, hi, seli, atexti, idi) {
    this.xb = xi;
    this.yb = yi;
    this.wb = wi;
    this.hb = hi;
    this.selb = seli;
    this.textb = atexti;
    this.idb = idi;
  }

  draw() {
    this.mousePressed();
    strokeWeight(3);
    if (this.selb) fill(0, 200, 0);
    else fill(0, 0, 200);
    if (this.over()) stroke(200, 0, 200);
    else stroke(0, 200, 200);
    rect(this.xb, this.yb, this.wb, this.hb);
    noStroke();
    fill(200);
    textSize(30);
    text(this.textb, this.xb + 3, this.yb + this.hb / 2 + 8);
  }

  over() {
    return (mouseX > this.xb & mouseX < this.xb + this.wb & mouseY > this.yb & mouseY < this.yb + this.hb);
  }

  mousePressed() {
    //    if (this.over() && mouseIsPressed) {
    if (this.over() && mymouseClicked) {
      mymouseClicked = false; // reset global
      this.selb = true;
      if (this.idb < 30) q_mysel = this.idb; // set a global 0 .. 29
    }
  }

} //___________________________________________________ end class

