//________________________________________ STEP 6: IMPLEMENTING EXTERNAL DATA (P5)
//........................................ in step 5 we make a state frame work with:
//........................................ introduction screen (start with key [1])
//........................................ select question with key [1] till [9]
//........................................ question page with the selected question, answer with keys [a] [b] or [c]
//........................................ answer page after answering the question
//........................................ reset function with key [space]
//........................................ + external data file at /assets/qa(_6).csv (31 lines & 30 questions)
//........................................ + table to copy the CSV data to, and used this for the questions page

//________________________________________ GLOBAL VARIABLES
let state = 0; //_________________________ decide starting screen
let question = -1; //_____________________ remember which question the user selected
let answer = ""; //_______________________ remember which answer the user gave
let diagp = true; //______________________ show diagnostic info

//________________________________________ Q & A FROM CSV FILE
//........................................ For clarity all global variables and functions related to the CSV file loading
//........................................ are grouped here together. We use 'preload' to make sure all data from the CSV
//........................................ file is loaded before 'setup' executes.
let table, rows, cols, qrow;
let good; //______________________________ contains correct answer for selected question
let infile = "assets/qa_6.csv";

function preload() { //___________________ PRELOAD
  //...................................... 'loadTable' is a method that reads all the content from the CSV file and turns
  //...................................... it into an object (which is a more complex type of variable). We use this   
  //...................................... object inside the functions 'questions' and 'setup_CSV'.
  table = loadTable(infile, "csv", "header");
}

function setup_CSV() {
  //...................................... 'setup_CSV' gets called in 'setup'
  //...................................... read table object and optional print to console
  if (diagp) print("\nload CSV file: qa.csv from assets");
  rows = table.getRowCount();
  if (diagp) print(rows + ' total rows in table');
  cols = table.getColumnCount()
  if (diagp) print(cols + ' total columns in table');

  for (let r = 0; r < rows; r++) {
    let temp = "";
    for (let c = 0; c < cols; c++) {
      temp += table.getString(r, c) + "_";
    }
    if (diagp) print(temp);
  }
}

function setup() { //_____________________ SETUP
  createCanvas(400, 400);
  setup_CSV(); //_________________________ load data from CSV file
  print("this sketch is part of a tutorial: http://kll.engineering-news.org/kllfusion01/articles.php?article_id=166");
}

function draw() { //______________________ DRAW
  background(200, 200, 0);
  textSize(12);
  if (state === 0)       intro();
  else {
    if (question == -1)  buttons();
    else {
      if (answer === "") questions();
      if (answer > "")   answers();
    }
  }
  textSize(12);
  if (diagp) text("state " + state + " , question " + question + " , answer " + answer, 10, height - 5);
  text("restart : key [space]", width - 115, height - 5);
}

function intro() { //_____________________ FUNCTIONS
  text("INTRODUCTION SCREEN \npress key [1] to start the question game", 20, 30);
}

function buttons() {
  background(150, 200, 0);
  if (state == 1) text("SELECT QUESTION \nhere we pick a question with key [1] till [9]", 20, 30);
}

function questions() {
  //...................................... The variable 'qrow' copies the entire row of a table, which contains 6 strings
  //...................................... in total (the rubric, the question, 3 possible answers, and the correct 
  //...................................... answer). Each string gets fetched from the row using 'qrow.getString'.
  background(100, 200, 100);
  if (state == 1) text("QUESTION " + question + "\nhere we show question " + question + ". Answer with [a][b][c]", 20, 30);
  if (question >= 1 && question <= 30) qrow = table.getRow(question - 1);
  let yl = 100;
  text("# "            + qrow.getString(0), 20, yl);
  yl += 35;
  textSize(30);
  text(" Rubric    : " + qrow.getString(1), 20, yl);
  yl += 35;
  text(" Question: "   + qrow.getString(2), 20, yl);
  yl += 35;
  text(" [a] ? "       + qrow.getString(3), 20, yl);
  yl += 35;
  text(" [b] ? "       + qrow.getString(4), 20, yl);
  yl += 35;
  text(" [c] ? "       + qrow.getString(5), 20, yl);
  yl += 35;
  good = qrow.getString(6); //____________ get correct answer (a, b or c) from 'qrow'
}

function answers() {
  //...................................... In the previous step the correct answer was always 'b'. Now we compare the
  //...................................... user's answer to the variable 'good'. The value of 'good' is fetched from the
  //...................................... variable 'qrow' inside the function 'questions'.
  if (answer == 'a' || answer == 'b' || answer == 'c') {
    if (state == 1 && answer == good) { // right answer
      background(0, 200, 200);
      text("RIGHT ANSWER", 20, 60);
    } else { //___________________________ wrong answer
      background(200, 0, 200);
      text("WRONG ANSWER", 20, 60);
      text("You answered " + answer + ", but the correct one is " + good, 20, 80);
    }
    text("restart the sketch using key [ space]", 20, 120);
  } else { //_____________________________ wrong key
    background(200, 0, 0);
    text("answer with [a], [b] or [c]", 20, 60);
  }
  if (state == 1) text("ANSWER " + question + "\nyour answer to question " + question + " was " + answer, 20, 30);
}

function keyPressed() { //________________ OPERATION
  if (state == 1 && question == -1) question = key;
  else if (state == 1 && question > -1) answer = key;

  if (state === 0)
    if (key == 1) state = 1;

  if (key == ' ') {
    state = 0;
    question = -1;
    answer = "";
    print("sketch has reset");
  }
}
