//________________________________________ STEP 4: COMBINING ARRAYS AND CLASS (P5)

//________________________________________ GLOBAL VARIABLES
//........................................ Because we use a class now, we need less global variables. All button variables
//........................................ (such as its x, y, w, h) are declared in the class 'Button' instead.  
//........................................ When you use a class it results in an instance. So in this sketch, where we're 
//........................................ making 30 buttons with a class, we end up with 30 instances of 'Button'.
let amount = 30;
let myButtons = []; //____________________ make an array for the buttons of type class 'Button'
let mysel = -1; //________________________ tracks the last selected button

function setup() { //_____________________ SETUP
  createCanvas(400, 400);
  strokeWeight(3);
  textSize(20);
  init_array();
  print("this sketch is part of a tutorial: http://kll.engineering-news.org/kllfusion01/articles.php?article_id=166");
}

function draw() { //______________________ DRAW 
  background(200, 200, 0);
  draw_buttons();
}

function init_array() { //________________ ARRAY SETUP FUNCTION
  //...................................... Just like the previous step we use 'init_array' to prepare the buttons. Instead
  //...................................... of adding these values to arrays however, we pass them to the class 'Button'. 
  //...................................... The class then uses these values to make an instance.
  let x0 = 15,
    y0 = 50,
    w = 55,
    h = 50,
    off = 8,
    grid = 6;
  for (let i = 0; i < amount; i++) {
    let xi = x0 + (i % grid) * (w + off);
    let yi = y0 + (floor(i / grid)) * (h + off);
    let wi = w;
    let hi = h;
    let seli = false;
    let texti = " " + ((1 + floor(i / grid)) * 200);
    let idi = i;
    myButtons.push(new Button(xi, yi, wi, hi, seli, texti, idi)); //__ make a 'Button' and add it to the button array
  }
}

function draw_buttons() { //______________ BUTTON DRAW FUNCTION
  //...................................... Inside 'draw' we call the function 'draw_buttons', which uses a for loop to go 
  //...................................... through all the button instances. To prevent multiple buttons to be selected at
  //...................................... the same time, it sets all button instances (except the one currently selected)
  //...................................... to 'false'. Right after it draws the button. 
  for (let i = 0; i < amount; i++) {
    if (i != mysel) myButtons[i].selb = false;
    myButtons[i].draw();
  }
}

class Button { //_________________________ CLASS
  //...................................... Inside 'init_array' we pass values to this class. The 'constructor' receives
  //...................................... these values and appoints them to that specific button instance. 
  constructor(xi, yi, wi, hi, seli, atexti, idi) {
    this.xb = xi;
    this.yb = yi;
    this.wb = wi;
    this.hb = hi;
    this.selb = seli;
    this.textb = atexti;
    this.idb = idi;
  }

  //...................................... The class also contains the methods 'draw', 'over', and 'mousePressed'. You can
  //...................................... see these as local functions of that class. If you want to call a method within 
  //...................................... your sketch, you have to type the class name & the method. So for instance, to 
  //...................................... call the method 'draw' for the first button, you type 'Button[0].draw()'.
  draw() {
    this.mousePressed();
    if (this.selb)       fill(0, 200, 0); //______ show button is selected
    else                 fill(0, 0, 200);
    if (this.over())     stroke(200, 0, 200); //__ show mouse hover over button
    else                 stroke(0, 200, 200);
    rect(this.xb, this.yb, this.wb, this.hb);
    noStroke();
    fill(200);
    text(this.textb, this.xb + 3, this.yb + this.hb / 2 + 8);
  }

  over() { //_____________________________ mouse over button
    return (mouseX > this.xb & mouseX < this.xb + this.wb & mouseY > this.yb & mouseY < this.yb + this.hb);
  }

  mousePressed() { //_____________________ OPERATION
    if (this.over() && mouseIsPressed) {
      this.selb = true;
      mysel = this.idb; //________________ changes the value of global 'mysel' to the selected button
    }
  }
} //______________________________________ end class
