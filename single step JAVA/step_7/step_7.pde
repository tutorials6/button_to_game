//_______________________________________________ STEP 7: extra code file (JAVA)

//............................................... in the p5.js example the idea to have function or class in a extra file.js
//............................................... what needs to be declaread in the index.html
//............................................... is worth a lesson, here in multi PDE processing JAVA structure not really,
//............................................... just to stay conform i copy that anyway.
//............................................... sorry about the lesson number "_7" behind the variables

//............................................... remark (1) see file myTools_7.pde

//_______________________________________________ GLOBAL VARIABLES 
Button_7 button_7 = new Button_7(10, 10, 40, 40); 

public void setup() {
  size(768, 500);
  strokeWeight(3);
  textSize(20);
  println("info: http://kll.engineering-news.org/kllfusion01/articles.php?article_id=166");
}

public void draw() {
  background(200, 200, 0);
  logo_7(width - 100, 20, 40, 0.015); // logo( x, y, radius, speed+delta-ang-per-frame ); // (1)
  button_7.draw(); //____________________________ (1)
}

public void keyPressed() {}

public void mousePressed() {}
