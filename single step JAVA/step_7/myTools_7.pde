/*
 myTools  KLL
 use extra pde file here in root
 name: myTools_7.pde
 idea is to develop it here ( master ) and download / upload to other projects.
 content: 
 classes declared:
 
 class Button_7(x,y,w,h)
 declare:
 button_7 = new Button_7(x,y,w,h);
 
 use:
 button.draw();
 
 functions to call:
 
 logo(x,y,radius,speed);
 
 
 */

//________________________________________________________ FUNCTION LOGO
float ang_7 = 0;

void logo_7(int x, int y, int r, float dang) {
  float d1 = 2 * r;
  float d2 = 2 * r / sqrt(2);
  ang_7 += dang; //__________________ animation
  push();
  fill(255); //____________________ fill same all 3 shapes
  strokeWeight(4);
  stroke(200, 200, 0); //__________ here same as main background gives a nice effect
  ellipseMode(CENTER);
  rectMode(CENTER);
  translate(x + r, y + r); //______ CENTER thinking
  push();
  rotate(-ang_7); //__________________ animate first bigger rect
  rect(0, 0, d1, d1);
  pop();
  ellipse(0, 0, d1, d1);
  rotate(ang_7); //_________________ animate second smaller rect  
  rect(0, 0, d2, d2);
  textAlign(CENTER, CENTER);
  textSize(30);
  fill(200, 200, 0);
  text("KLL", 0, -5);
  pop();
}

//_______________________________________________________ CLASS BUTTON
// this is not a real button code, just a rect as class, to show that classes can be stored here
// while they are used from main 

class Button_7 {
  int x, y, w, h;

  Button_7(int x, int y, int w, int h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }

  void draw() {
    push();
    //fill(200,200,200);
    noFill();
    stroke(200, 200, 200);
    strokeWeight(3);
    //noStroke();
    rect(this.x, this.y, this.w, this.h); 
    pop();
  }
}
