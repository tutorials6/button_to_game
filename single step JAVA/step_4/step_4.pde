//________________________________________ STEP 4: COMBINING ARRAYS AND CLASS (P5)

//________________________________________ GLOBAL VARIABLES
//........................................ Because we use a class now, we need less global variables. All button variables
//........................................ (such as its x, y, w, h) are declared in the class 'Button' instead.  
//........................................ When you use a class it results in an instance. So in this sketch, where we're 
//........................................ making 30 buttons with a class, we end up with 30 instances of 'Button'.
int amount = 30;
Button[] myButtons = new Button[amount]; // make an array for the buttons of type class 'Button'
int mysel = -1; //________________________ tracks the last selected button

void setup() { //_________________________ SETUP
  size(400, 400);
  strokeWeight(3);
  textSize(20);
  init_array();
  println("this sketch is part of a tutorial: http://kll.engineering-news.org/kllfusion01/articles.php?article_id=166");
}

void draw() { //__________________________ DRAW 
  background(200, 200, 0);
  draw_buttons();
}

void init_array() { //____________________ ARRAY SETUP FUNCTION
  //...................................... Just like the previous step we use 'init_array' to prepare the buttons. Instead
  //...................................... of adding these values to arrays however, we pass them to the class 'Button'. 
  //...................................... The class then uses these values to make an instance.
  int x0 = 15, y0 = 50, w = 55, h = 50, off = 8, grid = 6;
  for (int i = 0; i < amount; i++) {
    int xi = x0 + (i % grid) * (w + off);
    int yi = y0 + (floor(i / grid)) * (h + off);
    int wi = w;
    int hi = h;
    boolean seli = false;
    String texti = " " + ((1 + floor(i / grid)) * 200);
    int idi = i;
    myButtons[i] = new Button(xi, yi, wi, hi, seli, texti, idi); //__ make a 'Button' and add it to the button array
  }
}

void draw_buttons() { //__________________ BUTTON DRAW FUNCTION
  //...................................... Inside 'draw' we call the function 'draw_buttons', which uses a for loop to go 
  //...................................... through all the button instances. To prevent multiple buttons to be selected at
  //...................................... the same time, it sets all button instances (except the one currently selected)
  //...................................... to 'false'. Right after it draws the button. 
  for (int i = 0; i < amount; i++) {
    if (i != mysel) myButtons[i].selb = false;
    myButtons[i].draw();
  }
}

class Button { //_________________________ CLASS
  //...................................... Inside 'init_array' we pass values to this class. The 'constructor' receives
  //...................................... these values and appoints them to that specific button instance. 
  int xb,yb,wb,hb,idb;
  boolean selb = false;
  String textb;
  Button(int xi, int yi, int wi, int hi, boolean seli, String atexti, int idi) {
    this.xb = xi;
    this.yb = yi;
    this.wb = wi;
    this.hb = hi;
    this.selb = seli;
    this.textb = atexti;
    this.idb = idi;
  }

  //...................................... The class also contains the methods 'draw', 'over', and 'mousePressed'. You can
  //...................................... see these as local functions of that class. If you want to call a method within 
  //...................................... your sketch, you have to type the class name & the method. So for instance, to 
  //...................................... call the method 'draw' for the first button, you type 'Button[0].draw()'.
  void  draw() {
    this.mousePressed();
    if (this.selb)       fill(0, 200, 0); //______ show button is selected
    else                 fill(0, 0, 200);
    if (this.over())     stroke(200, 0, 200); //__ show mouse hover over button
    else                 stroke(0, 200, 200);
    rect(this.xb, this.yb, this.wb, this.hb);
    noStroke();
    fill(200);
    text(this.textb, this.xb + 3, this.yb + this.hb / 2 + 8);
  }

  boolean over() { //_____________________ mouse over button
    return (mouseX > this.xb & mouseX < this.xb + this.wb & mouseY > this.yb & mouseY < this.yb + this.hb);
  }

  void mousePressed() { //________________ OPERATION
    if (this.over() && mousePressed) {
      this.selb = true;
      mysel = this.idb; //________________ changes the value of global 'mysel' to the selected button
    }
  }
} //______________________________________ end class
