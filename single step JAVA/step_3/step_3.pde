//________________________________________ STEP 3: COMBINING ARRAYS AND FUNCTIONS (P5)
//........................................ after (STEP_1) hard coded values, we used (STEP_2) variables and now,
//........................................ for many buttons, use here (STEP_3) arrays. 
//________________________________________ GLOBAL VARIABLES
//........................................ In the global variables we decide the amount of buttons and prepare (empty) 
//........................................ arrays. You can let your sketch know a variable is an array by adding an
//........................................ opening and closing square bracket ('[]') as a value.
int amount = 30;
//........................................ Each button has an x, y, w, h, toggle state, and text. In this step we make a 
//........................................ separate array for each of these properties.
int[] xb = new int[amount];
int[] yb = new int[amount];
int[] wb = new int[amount];
int[] hb = new int[amount];
boolean[] togb = new boolean[amount];
String[] txtb = new String[amount];

void setup() { //_________________________ SETUP
  //...................................... Inside setup we call the function 'init_array', which fills the arrays with
  //...................................... values. Because we don't want to change the sizes or positions of the buttons
  //...................................... while the sketch runs, we only need to call this function once inside 'setup'.
  size(400, 400);
  strokeWeight(3);
  textSize(20);
  init_array(); //________________________ setup of the arrays
  print("this sketch is part of a tutorial: http://kll.engineering-news.org/kllfusion01/articles.php?article_id=166");
}

void draw() { //__________________________ DRAW
  //...................................... Inside 'draw' we use a for loop to count from 0 till 29. At each count it calls
  //...................................... the function 'myButton' and passes 6 values. When it does the number between 
  //...................................... these square brackets are all the same, meaning all passed values belong to the
  //...................................... same button. So when the for loop is at 15, it passes all values (x, y, w, h, 
  //...................................... toggle state, text) of the slot [15] in each array.
  background(200, 200, 0);
  for (int i = 0; i < amount; i++) myButton(xb[i], yb[i], wb[i], hb[i], togb[i], txtb[i]);
}

void init_array() { //____________________ ARRAY SETUP FUNCTION
  //...................................... The function 'init_array' contains  local variables, the rectangle GRID setup.
  //...................................... We use these in the for loop to calculate where each button should be placed (x & y) 
  //...................................... and the text of the button (txtb).
  int x0 = 15, y0 = 50, w = 55, h = 50, off = 8, grid = 6;
  for (int i = 0; i < amount; i++) {
    xb[i] = x0 + (i % grid) * (w + off);
    yb[i] = y0 + (floor(i / grid)) * (h + off);
    wb[i] = w;
    hb[i] = h;
    togb[i] = false;
    txtb[i] = " " + ((1 + floor(i / grid)) * 200);
  }
}

void myButton(int x, int y, int w, int h, boolean sel, String atext) { // BUTTON FUNCTION
  if (sel)                 fill(0, 200, 0);
  else                     fill(0, 0, 200);
  if (over(x, y, w, h))    stroke(200, 0, 200);
  else                     stroke(0, 200, 200);
  rect(x, y, w, h);
  noStroke();
  fill(200);
  text(atext, x + 3, y + h / 2 + 8);
}

boolean over(int x, int y, int w, int h) { // MOUSE OVER RECTANGLE
  return (mouseX > x & mouseX < x + w & mouseY > y & mouseY < y + h);
}

void mousePressed() { //__________________ OPERATION
  //...................................... We also use a for loop inside 'mousePressed'. Whenever the mouse button is 
  //...................................... pressed the sketch checks if the mouse cursor is at the same location as any of
  //...................................... the buttons. It passes the x, y, w, and h of each button (one at a time) to the 
  //...................................... function 'over', which then compares the values.
  //...................................... If 'over' returns the value 'true', then the toggle state (togb) of that button 
  //...................................... switches its value.
  for (int i = 0; i < amount; i++)
    if (over(xb[i], yb[i], wb[i], hb[i])) 
      togb[i] = !togb[i];
}
