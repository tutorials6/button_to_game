//______________________________________________ STEP 2: USING A FUNCTION TO MAKE MULTIPLE BUTTONS (P5)

//______________________________________________ GLOBAL VARIABLES
//.............................................. In this sketch all hard coded values of the button are replaced by global 
//.............................................. variables. 'w' is short for width and 'h' is short for height. Combining
//.............................................. global variables and functions allow us to make a second button (2) with
//.............................................. only 3 lines of code.
//.............................................. As an exercise you could try to add a third button yourself.

int x1 = 100, y1 = 100, w1 = 80, h1 = 30;
boolean tog1 = false;
int x2 = 100, y2 = 150, w2 = 80, h2 = 30;
boolean tog2 = false; //________________________ (2)

void setup() { //_______________________________ SETUP
  size(400, 400);
  strokeWeight(3);
  textSize(20);
  print("this sketch is part of a tutorial: http://kll.engineering-news.org/kllfusion01/articles.php?article_id=166");
}

void draw() { //________________________________ DRAW
  //............................................ Inside 'draw' we call the function 'myButton' twice (once for each
  //............................................ button). The only thing that's different is the values between the
  //............................................ brackets. These are the values that we pass to the function 'myButton'.
  background(200, 200, 0);
  myButton(x1, y1, w1, h1, tog1, "text 1");
  myButton(x2, y2, w2, h2, tog2, "text 2"); //__ (2)
}

void myButton(int x, int y, int w, int h, boolean tog, String txt) { //________ FUNCTIONS
  //............................................ The function 'myButton' draws a button with the values it receives. It
  //............................................ turns these values in local variables, which vanish once the function
  //............................................ 'myButton' has executed all its lines of code. The function 'over' checks
  //............................................ if the mouse cursor is hovering over a button. If so, it returns 'true'.
  if (tog)              fill(0, 200, 0);
  else                  fill(0, 0, 200);
  if (over(x, y, w, h)) stroke(200, 0, 200); //_ We call the function 'over' and pass 4 values of rectangle position and size
  else                  stroke(0, 200, 200);
  rect(x, y, w, h);
  noStroke();
  fill(200);
  text(txt, x + 10, y + h - 10);
}

boolean  over(int x, int y, int w, int h) { //__ check mouse over rect
  return (mouseX > x & mouseX < x + w & mouseY > y & mouseY < y + h);
}

void mousePressed() { //________________________ OPERATION
  //............................................ When the user clicks the mouse, the sketch checks if the mouse cursor is at the
  //............................................ same location as the buttons. As you can see it uses the same function as the  
  //............................................ the hover effect ('over').
  if (over(x1, y1, w1, h1)) tog1 = !tog1; //____ button toggle
  if (over(x2, y2, w2, h2)) tog2 = !tog2; //____ (2)
}
