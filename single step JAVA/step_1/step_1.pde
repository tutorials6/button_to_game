//________________________________________ STEP 1: HARD CODED TOGGLE BUTTON

//________________________________________ GLOBAL VARIABLES
//........................................ To hard code a button which we can toggle, we need a global variable to keep 
//........................................ track of the button's state. A boolean is perfect for that.

boolean tog = false; //___________________ but false is default, so ' = false ' not needed

void setup() { //_________________________ SETUP
  //...................................... Inside 'setup' we set the values screen and preset values that don't change
  //...................................... change throughout the sketch, such as the size of the text.
  size(400, 400);
  textSize(20);
  println("this is the start of a tutorial see: http://kll.engineering-news.org/kllfusion01/articles.php?article_id=166");
}

void draw() { //__________________________ DRAW
  //...................................... 'draw' is where we draw the button on the screen. By default the it refreshes
  //...................................... 60 frames per second. We also add a hover effect (a mouse over) by changing
  //...................................... the color of the button border.
  background(200, 200, 0);
  if (tog)     fill(0, 200, 0);
  else         fill(0, 0, 200);
  if (mouseX > 100 && mouseX < 180 && mouseY > 100 && mouseY < 130) stroke(200, 0, 200);
  else         stroke(0, 200, 200);
  strokeWeight(3);
  rect(100, 100, 80, 30); //______________ button as rectangle
  fill(200);
  text("press", 110, 120); //_____________ text placed on top of the button
}

void mousePressed() { //__________________ OPERATION
  //...................................... The function 'mousePressed' gets executed each time the user clicks the mouse
  //...................................... button. When that happens we could check if the mouse cursor is at the same
  //...................................... location as the rectangle that we placed in 'draw'. If so, we switch the state
  //...................................... of the global variable 'tog'.
  if (mouseX > 100 && mouseX < 180 && mouseY > 100 && mouseY < 130) {
    tog = !tog; //________________________ switch the button state from 'false' to 'true' or from 'true' to 'false'
    println("button " + tog); //__________ optional logging
  }
}
