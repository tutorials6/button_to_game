/*
 myTools  KLL
 use extra pde file here in root
 name: myTools_8.pde
 idea is to develop it here ( master ) and download / upload to other projects.
 content: 
 classes declared:
 
 __________________________________ classes declared:
 
 class Button
 declare:
 Button button(x, y, w, h, sel, text, id);
 
 methods:
 button.draw(); //_______________ from draw
 button.mousePressed(); //_______ local
 boolean button.over(); //_______ local
 
 gets:
 mymouseClicked_8 //_______________ a global set by mouse click
 sets:
 q_mysel_8 //______________________ a needed global for the 30 buttons page
 
 
 
 ___________________________________ functions to call:
 
 logo(x,y,radius,speed);
 
 
 */

//________________________________________________________ FUNCTION LOGO
float ang_8 = 0;

void logo_8(int x, int y, int r, float dang) {
  float d1 = 2 * r;
  float d2 = 2 * r / sqrt(2);
  ang_8 += dang; //__________________ animation
  push();
  fill(255); //____________________ fill same all 3 shapes
  strokeWeight(4);
  stroke(200, 200, 0); //__________ here same as main background gives a nice effect
  ellipseMode(CENTER);
  rectMode(CENTER);
  translate(x + r, y + r); //______ CENTER thinking
  push();
  rotate(-ang_8); //__________________ animate first bigger rect
  rect(0, 0, d1, d1);
  pop();
  ellipse(0, 0, d1, d1);
  rotate(ang_8); //_________________ animate second smaller rect  
  rect(0, 0, d2, d2);
  //textAlign(CENTER, CENTER);
  //textSize(20);
  //fill(200, 200, 0);
  //text("K L L", 0, 0);
  pop();
}

//_______________________________________________________ CLASS BUTTON
class Button_8 {   //____________________________________ begin class
  int x, y, w, h, id;
  boolean sel;
  String atext;

  Button_8(int x, int y, int w, int h, boolean sel, String atext, int id) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.sel = sel;
    this.atext = atext;
    this.id = id;
  }

  void  draw() {
    this.mousePressed();
    strokeWeight(3);
    if (this.sel) fill(0, 200, 0);
    else fill(0, 0, 200);
    if (this.over()) stroke(200, 0, 200);
    else stroke(0, 200, 200);
    rect(this.x, this.y, this.w, this.h);
    noStroke();
    fill(200);
    textSize(30);
    text(this.atext, this.x + 3, this.y + this.h / 2 + 8);
  }

  boolean  over() {
    return (mouseX > this.x & mouseX < this.x + this.w & mouseY > this.y & mouseY < this.y + this.h);
  }

  void  mousePressed() {
    //    if (this.over() && mouseIsPressed) {
    if (this.over() && mymouseClicked_8) {
      mymouseClicked_8 = false; // reset global
      this.sel = true;
      if (this.id < 30) q_mysel_8 = this.id; // set a global 0 .. 29
    }
  }
} //___________________________________________________ end class
