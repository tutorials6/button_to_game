//___________________________________________________________ TAB screen
class L_3 {
  int screen=3;
  // this version already for many buttons is using arrays for each variable
  // but again same functions for draw and over.

  int many = 30;
  int[] x = new int[many];
  int[] y = new int[many];
  int[] w = new int[many];
  int[] h = new int[many];
  boolean[] sel = new boolean[many];
  String[] text = new String[many];

  L_3() {
  }

  public void setup() {
    if ( diag ) println("setup class L_"+screen);
    init_array();
  }

  public void draw() {
    background(200, 200, 0);
    for (int i = 0; i < many; i++ )  myButton(x[i], y[i], w[i], h[i], sel[i], text[i]);
  }

  public void keyPressed() {
    if ( diag ) println("key press class L_"+screen+" "+key);
  }

  public void mousePressed() {
    if ( diag ) println("mouse press class L_"+screen);
    for (int i = 0; i < many; i++ )  
      if ( over(x[i], y[i], w[i], h[i]) ) sel[i] = ! sel[i];      // button i action
  }

  void init_array() {
    int x0=15, y0=50, w0 = 55, h0 = 50, off0 = 8, grid0 =6;  //_____ 6 cols * 5 rows = 30 buttons
    for (int i = 0; i < many; i++ ) {
      x[i] = x0 + (i % grid0) * (w0 + off0);
      y[i] = y0 + (floor(i / grid0)) * (h0 + off0);
      w[i] = w0;
      h[i] = h0;
      sel[i] = false;
      text[i]=" "+( (1+floor(i / grid0))*200);
    }
  }

  void myButton(int x, int y, int w, int h, boolean sel, String atext) {
    if ( sel )               fill(0, 200, 0);
    else                     fill(0, 0, 200);
    if ( over(x, y, w, h) )  stroke(200, 0, 200);
    else                     stroke(0, 200, 200);
    rect(x, y, w, h);
    noStroke();
    fill(200);
    text(atext, x+3, y+h/2+8);
  }

  boolean over(int x, int y, int w, int h) {
    return ( mouseX > x & mouseX < x + w &  mouseY > y & mouseY < y + h ) ;
  }
  
} //_ end class
