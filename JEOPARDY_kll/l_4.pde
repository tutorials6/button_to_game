//___________________________________________________________ TAB screen
class L_4 {
  int screen=4;
  // here we make a class and a array of buttons using it.
  // a class has 2 advantages, it has a own set of memory, with mixed variable types...
  // like a record struct
  // and it can have methods ( functions included )

  int many = 30;                                       // many buttons
  Button[] myButtons = new Button[many];               // make a array for buttons of type class Button
  int mysel = -1;                                      // global memory about the last selected button

  L_4() {
  }

  public void setup() {
    if ( diag ) println("setup class L_"+screen);
    init_array();
  }

  public void draw() {
    background(200, 200, 0);
    draw_buttons();
  }

  public void keyPressed() {
    if ( diag ) println("key press class L_"+screen+" "+key);
  }

  public void mousePressed() {
    if ( diag ) println("mouse press class L_"+screen);
  }


  class Button { //____________________________________ begin class
    int xb, yb, wb, hb, idb;
    boolean selb;
    String textb;

    Button(int xi, int yi, int wi, int hi, boolean seli, String atexti, int idi) {
      this.xb = xi;
      this.yb = yi;
      this.wb = wi;
      this.hb = hi;
      this.selb = seli;
      this.textb = atexti;
      this.idb = idi;
    }

    void  draw() {
      this.mousePressed();
      if (this.selb) fill(0, 200, 0);
      else fill(0, 0, 200);
      if (this.over()) stroke(200, 0, 200);
      else stroke(0, 200, 200);
      rect(this.xb, this.yb, this.wb, this.hb);
      noStroke();
      fill(200);
      text(this.textb, this.xb + 3, this.yb + this.hb / 2 + 8);
    }

    boolean  over() {
      return (mouseX > this.xb & mouseX < this.xb + this.wb & mouseY > this.yb & mouseY < this.yb + this.hb);
    }

    void  mousePressed() {
      if (this.over() && mousePressed ) {
        this.selb = true;
        mysel = this.idb; // set a global 0 ..
      }
    }
  } //_________________________________________________ end class

  void init_array() {
    int x0 = 15, y0 = 50, w = 55, h = 50, off = 8, grid = 6;
    for (int i = 0; i < many; i++) {
      int xi = x0 + (i % grid) * (w + off);
      int yi = y0 + (floor(i / grid)) * (h + off);
      int wi = w;
      int hi = h;
      boolean seli = false;
      String texti = " " + ((1 + floor(i / grid)) * 200);
      int idi = i;
      myButtons[i] = new Button(xi, yi, wi, hi, seli, texti, idi); // make buttons and save to array
    }
  }

  void draw_buttons() {
    for (int i = 0; i < many; i++) {
      if (i != mysel) myButtons[i].selb = false;           // only one ( last ) can be selected ( optionsgroup thinking )
      myButtons[i].draw();
    }
  }
} //_ end class
