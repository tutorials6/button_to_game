//___________________________________________________________ TAB screen
class L_2 {
  int screen=2;

  // using a function for the button and one for mouse over: 
  // please see how little change needed to make one more button  (2)

  int x1 = 100, y1 = 100, w1 =80, h1 =30;
  boolean sel1 = false;
  String text1 = "text1";

  int x2 = x1, y2 = y1+h1+10, w2 =w1, h2 =h1; //_________________ (2)
  boolean sel2 = false; //_______________________________________ (2)
  String text2 = "text2"; //_____________________________________ (2)

  L_2() {
  }

  public void setup() {
    if ( diag ) println("setup class L_"+screen);
  }

  public void draw() {
    background(200, 200, 0);
    myButton(x1, y1, w1, h1, sel1, text1);
    myButton(x2, y2, w2, h2, sel2, text2); //____________________ (2)
  }

  public void keyPressed() {
    if ( diag ) println("key press class L_"+screen+" "+key);
  }

  public void mousePressed() {
    if ( diag ) println("mouse press class L_"+screen);
    if ( over(x1, y1, w1, h1) ) sel1 = ! sel1;            // button 1 action
    if ( over(x2, y2, w2, h2) ) sel2 = ! sel2;            // button 2 action //_ (2)
  }


  void myButton(int x, int y, int w, int h, boolean sel, String atext) {
    if ( sel )               fill(0, 200, 0);
    else                     fill(0, 0, 200);
    strokeWeight(3);
    if ( over(x, y, w, h) )  stroke(200, 0, 200);
    else                     stroke(0, 200, 200);
    rect(x, y, w, h);
    noStroke();
    fill(200);
    text(atext, x+10, y+h-10);
  }

  boolean over(int x, int y, int w, int h) {
    return ( mouseX > x & mouseX < x + w &  mouseY > y & mouseY < y + h ) ;
  }
} //_ end class
