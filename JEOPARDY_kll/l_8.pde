//___________________________________________________________ TAB screen
public int q_mysel_8 = -1; //___________________________ global memory about the last selected button                               // used (1)
public boolean mymouseClicked_8 = false; //_____________ own boolean                                                                // used (1)

class L_8 {
  int screen=8;

  // this is the final 008 version of this tutorial where all is combined
  // it is not a real game, i even don't know that game
  // and would need lots of beauty work,
  // but you might call it a framework

  // remark // (1) see file myTools_8.pde

  int state = 0; //________________________________ show intro screen
  int last_question = -1; //_______________________ remember last question ( like button press 0 ..30 )
  char last_answer = ' '; //_______________________ remember last answer
  boolean diagp = true; //_________________________ show diagnostic info while development

  //_______________________________________________ Q & A from CSV file loading
  Table table;
  int rows, cols;
  TableRow qrow;
  String good; //______________________________ good answer for question in qrow of table
  String infile = "data/qa.csv";

  //_______________________________________________ category / rubric for 3 states to configure
  String[] cat1 = {"11", "12", "12", "14", "15", "16"};
  String[] cat2 = {"21", "22", "22", "24", "25", "26"};
  String[] cat3 = {"31", "32", "32", "34", "35", "36"};

  //_______________________________________________ question page button setup / variables
  int q_many = 30; //____________________________ many buttons
  Button_8[] q_myButtons = new Button_8[q_many]; //_________________________ make a array for buttons of type class Button            // (1)
  Button_8 state1, state2, state3, back, logit, aB, bB, cB; //__ operation buttons                                                    // (1)


  void table_load() { //____________________________ get the question list from CSV file in /assets/
    table = loadTable(infile, "csv, header");
  }

  L_8() {
  }

  public void setup() {
    if ( diag ) println("setup class L_"+screen);
    setup_CSV(); //________________________________ get questions_answers_list
    init_q_Button_array(); //______________________ question page setup 30 buttons
  }

  public void draw() {
    background(200, 200, 0);
    textSize(15);
    fill(0); //____________________________________ black help texts
    if (state == 0) intro();
    else {
      buttons$fromCSV(); //__________________________ rev b get $ from CSV ( for this state )
      if (last_question == -1)  buttons();
      else {
        if (last_answer == ' ') questions();
        else                      answers();
      }
    }
    textSize(15);
    if (diagp) text("state " + state + " , last_question " + last_question + " , last_answer " + last_answer + ", good " + good, 10, height - 5); //+", back : key [space]"
  }

  public void keyPressed() {
    if ( diag ) println("key press class L_"+screen+" "+key);
    if (state == 0) { //___________________________ INTRO PAGE  ( only for tests used )
      if (key == 1) state = 1;
      if (key == 2) state = 2;
      if (key == 3) state = 3;
    } else if (state > 0 && state < 4) { //______ QUESTION PAGE  ( only for tests used )
      if (last_question == -1)        last_question = int(key)-48;
      else if (last_question > -1)    last_answer = key;
    }
    if (key == ' ') {
      reset();
    }
  }

  public void mousePressed() {
    if ( diag ) println("mouse press class L_"+screen);
    mymouseClicked_8 = true;
  }

  void intro() {
    textSize(20);
    //text("INTRO \n press key [1][2][3] or Button for start questions game", 20, 30);
    logo_8(width - 100, 20, 40, 0.015); // logo( x, y, radius, speed-delta-ang-per-frame );                                        // (1)
    state1.draw(); //__________________________________________________ show select buttons
    if (state1.sel) state = 1;
    state2.draw();
    if (state2.sel) state = 2;
    state3.draw();
    if (state3.sel) state = 3;
  }

  void buttons() {
    background(150, 200, 0);
    //___________________________________ we know state ( and it is > 0 ) so we know what to show
    textSize(20);
    //text("SCREEN " + state + " here we SELECT questions 0 .. 29, selected " + q_mysel_8, 20, 20);
    fill(200, 0, 0);
    if (state == 1)
      for (int j = 0; j < 6; j++) text(cat1[j], 20 + j * 123, 40);
    if (state == 2)
      for (int j = 0; j < 6; j++) text(cat2[j], 20 + j * 123, 40);
    if (state == 3)
      for (int j = 0; j < 6; j++) text(cat3[j], 20 + j * 123, 40);
    draw_q_buttons();
    back.draw();
    logit.draw();
    if (logit.sel) last_question = q_mysel_8; //________ go screen question
    if (back.sel) reset(); //__________________________ go start screen
  }

  void questions() {
    background(100, 200, 100);
    //text("SCREEN " + state + " here we ASK question " + last_question + "\nonly answer with [a][b][c]", 20, 30);
    //____________________ select from 90 CSV lines by last_question+(state-1)*30
    if (last_question >= 0 && last_question < 30) qrow = table.getRow(last_question + (state - 1) * 30);
    int yl = 100;
    text("# " + qrow.getString(0), 20, yl);
    yl += 35;
    textSize(30);
    text(" Rubric       :" + qrow.getString(1), 20, yl);
    yl += 35;
    text(" Question: " + qrow.getString(2), 20, yl);
    yl += 35;
    text(" [a] ? " + qrow.getString(3), 20, yl);
    yl += 35;
    text(" [b] ? " + qrow.getString(4), 20, yl);
    yl += 35;
    text(" [c] ? " + qrow.getString(5), 20, yl);
    yl += 35;
    good = qrow.getString(6); // a b c 
    //_________________________________________________________________ buttons
    aB.draw();
    if (aB.sel) last_answer = 'a'; //______________________________ or key press
    bB.draw();
    if (bB.sel) last_answer = 'b';
    cB.draw();
    if (cB.sel) last_answer = 'c';
  }

  void answers() {
    if (last_answer == 'a' || last_answer == 'b' || last_answer == 'c') {
      if ( str(last_answer).equals(good) ) { //_____________________/ RIGHT
        background(0, 200, 200);
        textSize(50);
        text(" WIN ", 20, 160);
        textSize(100);
        //      text("$ "+(1 + floor(last_question / 6)) * 200+" $"  , 20,300);
        //      text("$ "+q_myButtons[last_question].atext+" $"  , 20,300);
        text("$ " + table.getString(last_question + (state - 1) * 30, 7) + " $", 20, 300);
      } else { //___________________________________________________________/ WRONG
        background(200, 0, 200);
        textSize(50);
        text(" LOSE ", 20, 160);
        textSize(20);
        text("last_answer " + last_answer + " good " + good, 40, 190);
      }
      back.draw();
      if (back.sel) reset(); //__________________________ go start screen
    } else { //_____________________________________________________________/ TYPO ?
      background(200, 0, 0);
    }
    textSize(20);
    //text("ANSWERS state " + state + " check: your answer to q " + last_question + " was " + last_answer+ " good " + good, 20, 30);
  }

  void reset() {
    state = 0;
    last_question = -1;
    last_answer = ' ';
    good ="";
    q_mysel_8 = -1; //______________________________ reset also last selected operation buttons
    back.sel = false;
    logit.sel = false;
    state1.sel = false;
    state2.sel = false;
    state3.sel = false;
    aB.sel = false;
    bB.sel = false;
    cB.sel = false;
    //println("RESET");
  }

  void setup_CSV() {
    table_load(); //_______________________________ load file
    if (diagp) print("\nload CSV file: qa.csv from data");
    rows = table.getRowCount();
    if (diagp) println(rows + " total rows in table");
    cols = table.getColumnCount();
    if (diagp) println(cols + " total columns in table");
    String headers = "";
    for (int j = 0; j < cols; j++) headers += table.getColumnTitle(j) + "_";
    println(headers);
    for (int r = 0; r < rows; r++) {
      String temp = "";
      for (int c = 0; c < cols; c++) {
        temp += table.getString(r, c) + "_";
      }
      if (diagp) println(temp);
    }
  }


  //________________________________________________________ question Button setup
  void init_q_Button_array() {
    int x0 = 15, 
      y0 = 50, 
      w = 115, 
      h = 70, 
      off = 8, 
      grid = 6;
    for (int i = 0; i < q_many; i++) {
      int xi = x0 + (i % grid) * (w + off);
      int yi = y0 + (floor(i / grid)) * (h + off);
      int wi = w;
      int hi = h;
      boolean seli = false;
      //    String texti = " " + ((1 + floor(i / grid)) * 200);   // rev a
      String texti = " " + i; // rev b
      //    String texti = " " + table.getString(i+(state-1)*30,7);  // rev, get win $ from CSV NOT WORK in this state!
      int idi = i;
      q_myButtons[i]= new Button_8(xi, yi, wi, hi, seli, texti, idi); // make buttons and save to array
    }

    //________________________________________________________________________________ from page intro
    state1 = new Button_8(width / 2 - 250, height / 2, 150, 40, false, "Round 1", 95); //__
    state2 = new Button_8(width / 2 - 75, height / 2, 150, 40, false, "Round 2", 96); //__
    state3 = new Button_8(width / 2 + 100, height / 2, 150, 40, false, "Tie Break", 97); //__
    //________________________________________________________________________________ from page buttons
    logit = new Button_8(width / 2, height - 42, 80, 40, false, " SEL", 98); //____________ from question select
    back = new Button_8(width - 85, height - 42, 80, 40, false, "back", 99); //___________ as key [space]
    //________________________________________________________________________________ from page question multiple choice
    aB = new Button_8(width / 2 - 250, height / 2 + 100, 150, 40, false, "[a]", 92); //____________ [a]
    bB = new Button_8(width / 2 - 75, height / 2 + 100, 150, 40, false, "[b]", 93); //____________ [b]
    cB = new Button_8(width / 2 + 100, height / 2 + 100, 150, 40, false, "[c]", 94); //____________ [c]
  }

  void buttons$fromCSV() {
    if (state >= 0) {
      for (int i = 0; i < q_many; i++) {
        String texti = " " + table.getString(i + (state - 1) * 30, 7); // rev, get win $ from CSV
        q_myButtons[i].atext = texti;
      }
    } else println("state error");
  }

  void draw_q_buttons() {
    for (int i = 0; i < q_many; i++) {
      if (i != q_mysel_8) q_myButtons[i].sel = false; // only one ( last ) can be selected ( optionsgroup thinking )
      q_myButtons[i].draw();
    }
  }
} //_ end class
