//___________________________________________________________ TAB screen
class L_7 {
  int screen=7;

  // in the p5.js example the idea to have function or class in a extra file.js
// what needs to be declaread in the index.html
// is worth a lesson, here in multi PDE processing JAVA structure not realy,
// just to stay conform i copy that anyway.

// sorry about the lesson number "_7" behind the variables

// remark (1) see file myTools_7.pde

Button_7 button_7 = new Button_7(10, 10, 40, 40); 


L_7() {
  }

public void setup() {
    if ( diag ) println("setup class L_"+screen);
  }

public void draw() {
  background(200, 200, 0);
  logo_7(width - 100, 20, 40, 0.015); // logo( x, y, radius, speed+delta-ang-per-frame ); // (1)
  button_7.draw(); // (1)
  }

public void keyPressed() {
    if ( diag ) println("key press class L_"+screen+" "+key);
  }

public void mousePressed() {
    if ( diag ) println("mouse press class L_"+screen);
  }
  
} //_ end class
