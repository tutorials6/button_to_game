//___________________________________________________________ TAB screen
class L_6 {
  int screen=6;

  /*_______________________________________________ Q & A from CSV file loading
   a state frame work with
   intro page start [1] back [space]
   button page [1] [9] later 30 buttons
   a question page for the selected question multipe choice [a][b][c]
   a answer page after the answer a b c check if correct for state & question, or wrong or typo
   
   added a external file /data/qa.csv ( 31 lines / 30 questions )
   read to a table and used for the questions page
   */

  Table table;
  TableRow qrow;

  int rows=0, cols=0;

  String good; //_____________________________________ good answer for question in qrow
  String infile = "data/qa.csv";

  int state = 0; //___________________________________ show intro screen
  int last_question = -1; //__________________________ remember last question ( like button press 0 ..30 )
  char last_answer = ' '; //__________________________ remember last answer
  boolean diagp = true; //____________________________ show diagnostic info while development

  L_6() {
  }

  public void setup() {
    if ( diag ) println("setup class L_"+screen);
    table_load();
    setup_CSV();
  }

  public void draw() {
    background(200, 200, 0);
    if (state == 0) intro();
    else {
      if (last_question == -1)  buttons();
      else {
        if (last_answer == ' ') questions();
        else                      answers();
      }
    }
    textSize(20);
    if (diagp) text("state " + state + " , last_question " + last_question + " , last_answer '" + last_answer+"'", 10, height - 5);
    text("back : key [space]", width - 200, height - 5);
  }

  public void keyPressed() {
    if ( diag ) println("key press class L_"+screen+" "+key);
    //println("key "+key+" keyCode "+keyCode);
    if      (state == 1 && last_question == -1) last_question = int(key)-48;
    else if (state == 1 && last_question >  -1) last_answer = key;

    if (state == 0)
      if (key == '1') state = 1;

    if (key == ' ') { //____________________________  [ ] reset
      state = 0;
      last_question = -1;
      last_answer = ' ';
      if (diagp) println("RESET");
    }
  }

  public void mousePressed() {
    if ( diag ) println("mouse press class L_"+screen);
  }

  void table_load() {
    table = loadTable(infile, "csv, header");
  }

  void setup_CSV() {
    if (diagp) println("\nload CSV file: qa.csv from assets");
    rows = table.getRowCount();
    if (diagp) println(rows + " total rows in table");
    cols = table.getColumnCount();
    if (diagp) println(cols + " total columns in table");

    for (int r = 0; r < rows; r++) {
      String temp = "";
      for (int c = 0; c < cols; c++) {
        temp += table.getString(r, c) + "_";
      }
      if (diagp) println(temp);
    }
  }

  void intro() {
    text("INTRO \n press key [1] for start questions game 1", 20, 30);
  }

  void buttons() {
    background(150, 200, 0);
    //___________________________________ we know state ( and it is > 0 ) so we know what to show
    if (state == 1) text("SCREEN " + state + " here we SELECT questions 1 .. 9", 20, 30);
  }

  void questions() {
    background(100, 200, 100);
    if (state == 1) text("SCREEN " + state + " here we ASK question " + last_question + "\nonly answer with [a][b][c]", 20, 30);
    if (last_question >= 1 && last_question <= 30) {
      //println("get row "+(last_question - 1) );
      qrow = table.getRow(last_question - 1);
    }
    int y5 = 120;
    text("# " + qrow.getString(0), 20, y5);
    y5 += 35;
    textSize(30);
    text(" Rubric       :" + qrow.getString(1), 20, y5);
    y5 += 35;
    text(" Question: " + qrow.getString(2), 20, y5);
    y5 += 35;
    text(" [a] ? " + qrow.getString(3), 20, y5);
    y5 += 35;
    text(" [b] ? " + qrow.getString(4), 20, y5);
    y5 += 35;
    text(" [c] ? " + qrow.getString(5), 20, y5);
    y5 += 35;
    good = qrow.getString(6); // a b c
  }

  void answers() {
    if (last_answer == 'a' || last_answer == 'b' || last_answer == 'c' ) { //____  limit to a b c 
      if (state == 1 && str(last_answer).equals(good) ) { //_____________________/ RIGHT
        background(0, 200, 200);
        text(" WIN ", 20, 60);
      } // just for test use a fix good answer
      else { //__________________________________________________________________/ WRONG
        background(200, 0, 200);
        text(" LOSE ", 20, 60);
      }
    } else { //__________________________________________________________________/ TYPO ?
      background(200, 0, 0);
    }
    text("ANSWERS state " + state + " check: your answer to question " + last_question + " was " + last_answer, 20, 30);
  }
} //_ end class
