//___________________________________________________________ TAB screen
class L_5 {
  int screen=5;

  // this is independent from the prior lessons,
  // here we do a basic setup for a game with several screens / states 
  // only keyboard operation,
  // state 1 ( one game only )
  // question 1 .. 9
  // answers [a] [b] c[] // WIN with [b]

  int state = 0; //__________________________ show intro screen
  int last_question = -1; //_________________ remember last question ( like button press 0 ..30 )
  char last_answer = ' '; //_________________ remember last answer
  boolean diagp = true; //___________________ show diagnostic info while develop code


  L_5() {
  }

  public void setup() {
    if ( diag ) println("setup class L_"+screen);
    fill(0, 0, 200);
  }

  public void draw() {
    background(200, 200, 0);
    if (state == 0) intro();
    else {
      if (last_question == -1) buttons();
      else {
        if (last_answer == ' ') questions();
        else                   answers();
      }
    }
    if (diagp) text("state " + state + " , last_question " + last_question + " , last_answer '" + last_answer+"'", 10, height - 5);
    text("back : key [space]", width - 200, height - 5);
  }

  public void keyPressed() {
    if ( diag ) println("key press class L_"+screen+" "+key);
    println("key "+key+" keyCode "+keyCode);
    if      (state == 1 && last_question == -1) last_question = int(key)-48;
    else if (state == 1 && last_question >  -1) last_answer = key;

    if (state == 0)
      if (key == '1') state = 1;

    if (key == ' ') { //____________________________  [ ] reset
      state = 0;
      last_question = -1;
      last_answer = ' ';
      if (diagp) println("RESET");
    }
  }

  public void mousePressed() {
    if ( diag ) println("mouse press class L_"+screen);
  }

  void intro() {
    text("INTRO \n press key [1] for start questions game 1", 20, 30);
  }

  void buttons() {
    background(150, 200, 0);
    //___________________________________ we know state ( and it is > 0 ) so we know what to show
    if (state == 1) text("SCREEN " + state + " here we SELECT questions 1 .. 9", 20, 30);
  }

  void questions() {
    background(100, 200, 0);
    if (state == 1) text("SCREEN " + state + " here we ASK question " + last_question + "\nonly answer with [a][b][c]", 20, 30);
    text("the most important question of all?", 20, 150);
    text("[a]?", 20, 180);
    text("[b]?", 20, 210);
    text("[c]?", 20, 240);
  }

  void answers() {
    if (last_answer == 'a' || last_answer == 'b' || last_answer == 'c' ) {
      if (state == 1 && last_answer == 'b') { //_____________________________/ RIGHT
        background(0, 200, 200);
        text(" WIN ", 20, 60);
      } // just for test use a fix good answer
      else { //__________________________________________________________________/ WRONG
        background(200, 0, 200);
        text(" LOSE ", 20, 60);
      }
    } else { //__________________________________________________________________/ TYPO ?
      background(200, 0, 0);
    }
    text("ANSWERS state " + state + " check: your answer to question " + last_question + " was " + last_answer, 20, 30);
  }
} //_ end class
