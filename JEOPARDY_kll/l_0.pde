//___________________________________________________________ TAB screen
class L_0 {
  int screen=0;
  L_0() {
  }
// This is a test comment.
public void setup() {
    if ( diag ) println("setup class L_"+screen);
  }

public void draw() {
  background(200, 200, 0);
  fill(0, 200, 0);
  textSize(20);
  int ypos = 40,dy=35;
  text("this is the start of a tutorial see also ", 20, ypos); ypos+=dy;
  text("http://kll.engineering-news.org/kllfusion01/articles.php?article_id=166", 20, ypos); ypos+=dy;
  fill(200, 0, 200);
  text(" Start: please use arrow keys [Right] [Left]", 20, ypos); ypos+=dy;
  fill(0, 200, 0);
  text(" (1) hard coded button", 20, ypos); ypos+=dy;
  text(" (2) button and mouse over function", 20, ypos); ypos+=dy;
  text(" (3) array for button parameter", 20, ypos); ypos+=dy;
  text(" (4) array of button class", 20, ypos); ypos+=dy;
  text(" (5) game screens", 20, ypos); ypos+=dy;
  text(" (6) game screens with question CSV file", 20, ypos); ypos+=dy;
  text(" (7) button class form extra file", 20, ypos); ypos+=dy;
  text(" (8) ALL IN ONE", 20, ypos); ypos+=dy;
  }

public void keyPressed() {
    if ( diag ) println("key press class L_"+screen+" "+key);
  }

public void mousePressed() {
    if ( diag ) println("mouse press class L_"+screen);
  }
} //_ end class
