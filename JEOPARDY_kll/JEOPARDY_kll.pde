// class structure for a multi screen tutorial
/*
 KLL enginering 2019
 related to a series of p5.js codes
 in form of a short tutorial
 * about buttons ( from hardcoded to arrayList of class )and
 * screens for a game
 
 http://kll.engineering-news.org/kllfusion01/articles.php?article_id=166
 i try to pack all that here as a JAVA lesson pack.
 */

L_0 l_0;
L_1 l_1;
L_2 l_2;
L_3 l_3;
L_4 l_4;
L_5 l_5;
L_6 l_6;
L_7 l_7;
L_8 l_8;

int lesson = 0; //____________________________________ now 0 = start, lesson 1 .. 8
boolean init = false; //______________________________ used to force a l_x.setup();
boolean diag = true; //_______________________________ diagnostic prints

void setup() {
  size(768, 500);
  strokeWeight(3);
  textSize(20);
  println("for copy: http://kll.engineering-news.org/kllfusion01/articles.php?article_id=166");

  l_0 = new L_0(); //_________________________________ declare here so class can use width / height 
  l_1 = new L_1();
  l_2 = new L_2();
  l_3 = new L_3();
  l_4 = new L_4();
  l_5 = new L_5();
  l_6 = new L_6();
  l_7 = new L_7();
  l_8 = new L_8();

}

void draw() {
  background(200, 200, 0);
  String tit = "";
  if ( lesson >= 1 ) tit ="<|-- Lesson "+lesson+" --|>";
  else               tit ="<|-- Start --|>";
  surface.setTitle(tit);
  lesson_setup(); //__________________________________ only if ( init )
  if      ( lesson == 0 ) l_0.draw();
  else if ( lesson == 1 ) l_1.draw();
  else if ( lesson == 2 ) l_2.draw();
  else if ( lesson == 3 ) l_3.draw();
  else if ( lesson == 4 ) l_4.draw();
  else if ( lesson == 5 ) l_5.draw();
  else if ( lesson == 6 ) l_6.draw();
  else if ( lesson == 7 ) l_7.draw();
  else if ( lesson == 8 ) l_8.draw();
}

void lesson_setup() {
  if ( init ) {
    if      ( lesson == 0 )  l_0.setup(); //__________ tab l_0
    else if ( lesson == 1 )  l_1.setup(); //__________ tab l_1
    else if ( lesson == 2 )  l_2.setup(); //__________ tab l_2
    else if ( lesson == 3 )  l_3.setup(); //__________ tab l_3
    else if ( lesson == 4 )  l_4.setup(); //__________ tab l_4
    else if ( lesson == 5 )  l_5.setup(); //__________ tab l_5
    else if ( lesson == 6 )  l_6.setup(); //__________ tab l_6
    else if ( lesson == 7 )  l_7.setup(); //__________ tab l_7
    else if ( lesson == 8 )  l_8.setup(); //__________ tab l_8
  }
  init = false;
}

void keyPressed() {
  if ( keyCode == RIGHT || keyCode == LEFT ) {
    if ( keyCode == RIGHT ) { 
      lesson++; 
      init = true; //_________________________________ setup for each new lesson
    }
    if ( keyCode == LEFT )  lesson--;
    lesson = constrain(lesson, 0, 8);
    if ( diag ) println("lesson: "+lesson);
  }
  if      ( lesson == 0 ) l_0.keyPressed();
  else if ( lesson == 1 ) l_1.keyPressed();
  else if ( lesson == 2 ) l_2.keyPressed();
  else if ( lesson == 3 ) l_3.keyPressed();
  else if ( lesson == 4 ) l_4.keyPressed();
  else if ( lesson == 5 ) l_5.keyPressed();
  else if ( lesson == 6 ) l_6.keyPressed();
  else if ( lesson == 7 ) l_7.keyPressed();
  else if ( lesson == 8 ) l_8.keyPressed();
}

void mousePressed() {
  if      ( lesson == 0 ) l_0.mousePressed();
  else if ( lesson == 1 ) l_1.mousePressed();
  else if ( lesson == 2 ) l_2.mousePressed();
  else if ( lesson == 3 ) l_3.mousePressed();
  else if ( lesson == 4 ) l_4.mousePressed();
  else if ( lesson == 5 ) l_5.mousePressed();
  else if ( lesson == 6 ) l_6.mousePressed();
  else if ( lesson == 7 ) l_7.mousePressed();
  else if ( lesson == 8 ) l_8.mousePressed();
}
