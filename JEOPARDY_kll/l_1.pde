//___________________________________________________________ TAB screen
class L_1 {
  int screen=1;
  boolean sel = false;
  L_1() {
  }

  public void setup() {
    if ( diag ) println("setup class L_"+screen);
  }

  public void draw() {
    background(200, 200, 0);
    if (sel)   fill(0, 200, 0);
    else       fill(0, 0, 200);
    if (mouseX > 100 & mouseX < 180 & mouseY > 100 & mouseY < 130) stroke(200, 0, 200);
    else                                                           stroke(0, 200, 200);
    strokeWeight(3);
    rect(100, 100, 80, 30);
    noStroke();
    fill(200);
    text("press", 110, 120);
  }

  public void keyPressed() {
    if ( diag ) println("key press class L_"+screen+" "+key);
  }

  public void mousePressed() {
    if ( diag ) println("mouse press class L_"+screen);
    if (mouseX > 100 & mouseX < 180 & mouseY > 100 & mouseY < 130)  sel = !sel; // button action
  }
} //_ end class
